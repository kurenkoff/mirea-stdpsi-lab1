FROM alpine:3.10

RUN apk update && apk add ca-certificates

WORKDIR /opt

COPY srv /opt/

ENTRYPOINT ["/opt/srv"]