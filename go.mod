module mirea/stdpsi/lab1

go 1.13

require (
	github.com/AbiosGaming/push-api-client v0.3.0 // indirect
	github.com/fatih/color v1.9.0 // indirect
	github.com/gofrs/uuid v3.2.0+incompatible // indirect
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/websocket v1.4.1 // indirect
	github.com/hokaccha/go-prettyjson v0.0.0-20190818114111-108c894c2c0e // indirect
	github.com/spf13/pflag v1.0.5 // indirect
)
